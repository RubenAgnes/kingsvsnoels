package cat.escolapia.damviod.pmdm.KingsVsNoels;


import java.util.Random;

/**
 * Created by ruben.agnes on 11/01/2017.
 */
public class Board {
    static final int BOARD_WIDTH = 10;
    static final int BOARD_HEIGHT = 14;
    static final float TICK_INITIAL = 0.3f;

    public King kings[] = new King[3];
    public Row rows[] = new Row[13];
    public boolean gameOver = false;
    public int score = 0;
    public int king_id = 0;
    public int king_row = 0;

    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public Board() {
        placeRows();
        placeKing();
    }

    private void placeKing() {
        King king = new King(5, 14);
        king_row = king.y;
        kings[king_id] = king;
    }

    private void placeRows()
    {
        for(int i = 0; i < rows.length; i++)
        {
            boolean _dir;
            _dir = random.nextBoolean();
            Row row = new Row(0,i+1,_dir);
            rows[i] = row;
            int _x1 = random.nextInt(10);
            int _x2 = random.nextInt(10);
            rows[i].createNoels(_x1, _x2);
        }
    }

    public void update(float deltaTime) {
        if (gameOver) return;
        tickTime += deltaTime;
        while(tickTime > tick) {
            tickTime -= tick;
            for (int i = 0; i < rows.length; i++) {
                rows[i].update();
            }
        }
        king_row = kings[king_id].y;
        if(king_row < 14 && king_row > 0){
            if (kings[king_id].checkXoca(rows[king_row-1])) {
                if (king_id < 2) {
                    king_id += 1;
                    placeKing();
                } else {
                    gameOver = true;
                    return;
                }
            }
        }
        if(kings[king_id].finished){
            if (king_id < 2) {
                king_id += 1;
                placeKing();
            } else {
                gameOver = true;
                return;
            }
        }
    }

    public King getKing(){
        return kings[king_id];
    }
}
