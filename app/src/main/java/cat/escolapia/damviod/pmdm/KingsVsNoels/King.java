package cat.escolapia.damviod.pmdm.KingsVsNoels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruben.agnes on 11/01/2017.
 */
public class King {
    public int x, y;
    boolean dead = false;
    boolean finished = false;

    public King(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void turnLeft() {
        if(this.x > 0) {
            this.x -= 1;
        }
    }

    public void turnRight() {
        if(this.x < 9) {
            this.x += 1;
        }
    }

    public void turnUp() {
        if(this.y > 0) {
            if(this.y == 1){
                if(this.x == 1 || this.x == 4 || this.x == 5 || this.x == 8) {
                    this.y -= 1;
                    finished = true;
                }
            }
            else
            {
                this.y -= 1;
            }
        }
    }

    public void turnDown() {
        if(this.y < 14) {
            this.y += 1;
        }
    }

    public boolean checkXoca(Row row) {
        boolean xoc = false;
        for(int i = 0; i < row.noels.length; i++){
            if(this.x == row.noels[i].x){
                xoc = true;
                this.dead = true;
            }
        }
        return xoc;
    }
}
