package cat.escolapia.damviod.pmdm.KingsVsNoels;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Graphics.PixmapFormat;
import cat.escolapia.damviod.pmdm.framework.Screen;

public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }

    public void update(float deltaTime) {
        Graphics g = game.getGraphics();

        Assets.background = g.newPixmap("background.jpg", PixmapFormat.RGB565);
        Assets.logo = g.newPixmap("logo.png", PixmapFormat.ARGB4444);
        Assets.mainMenu = g.newPixmap("mainmenu.png", PixmapFormat.ARGB4444);
        Assets.buttons = g.newPixmap("buttons.png", PixmapFormat.ARGB4444);
        Assets.numbers = g.newPixmap("numbers.png", PixmapFormat.ARGB4444);
        Assets.ready = g.newPixmap("ready.png", PixmapFormat.ARGB4444);
        Assets.pause = g.newPixmap("pausemenu.png", PixmapFormat.ARGB4444);
        Assets.gameOver = g.newPixmap("gameover.png", PixmapFormat.ARGB4444);
        Assets.king1 = g.newPixmap("king1.png", PixmapFormat.ARGB4444);
        Assets.king2 = g.newPixmap("king2.png", PixmapFormat.ARGB4444);
        Assets.king3 = g.newPixmap("king3.png", PixmapFormat.ARGB4444);
        Assets.noel_right = g.newPixmap("noel_r.png", PixmapFormat.ARGB4444);
        Assets.noel_left = g.newPixmap("noel_l.png", PixmapFormat.ARGB4444);
        Assets.row = g.newPixmap("row.png", PixmapFormat.ARGB4444);
        Assets.throne = g.newPixmap("throne.png", PixmapFormat.ARGB4444);
        Assets.chest = g.newPixmap("chest.png", PixmapFormat.ARGB4444);

        Assets.click = game.getAudio().newSound("click.ogg");
        Assets.eat = game.getAudio().newSound("eat.ogg");
        Assets.xoc = game.getAudio().newSound("bitten.ogg");

        game.setScreen(new MainMenuScreen(game));

    }
    
    public void render(float deltaTime) {

    }

    public void pause() {

    }

    public void resume() {

    }

    public void dispose() {

    }
}
