package cat.escolapia.damviod.pmdm.KingsVsNoels;

import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;

public class Assets {
    public static Pixmap background;
    public static Pixmap logo;
    public static Pixmap mainMenu;
    public static Pixmap buttons;
    public static Pixmap numbers;
    public static Pixmap ready;
    public static Pixmap pause;
    public static Pixmap gameOver;
    public static Pixmap king1;
    public static Pixmap king2;
    public static Pixmap king3;
    public static Pixmap noel_right;
    public static Pixmap noel_left;
    public static Pixmap row;
    public static Pixmap throne;
    public static Pixmap chest;

    public static Sound click;
    public static Sound eat;
    public static Sound xoc;
}
