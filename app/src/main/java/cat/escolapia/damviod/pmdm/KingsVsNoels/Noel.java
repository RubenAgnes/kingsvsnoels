package cat.escolapia.damviod.pmdm.KingsVsNoels;

/**
 * Created by ruben.agnes on 11/01/2017.
 */
public class Noel {
    public int x, y;

    public Noel(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void move(boolean dir){
        if(dir){
            this.x++;
        }
        else{
            this.x--;
        }

        if(this.x > 10)
        {
            this.x = 0;
        }
        if(this.x < 0){
            this.x = 10;
        }
    }
}
