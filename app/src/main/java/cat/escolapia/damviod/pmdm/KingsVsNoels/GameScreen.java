package cat.escolapia.damviod.pmdm.KingsVsNoels;

import java.util.List;

import android.graphics.Color;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Screen;

public class GameScreen extends Screen {
    enum GameState {
        Ready,
        Running,
        Paused,
        GameOver
    }

    int xi=0;
    int yi=0;
    int xf=0;
    int yf=0;
    int distancex=0;
    int distancey=0;

    GameState state = GameState.Ready;
    Board board;
    int oldScore = 0;
    String score = "0";

    public GameScreen(Game game) {
        super(game);
        board = new Board();
    }

    @Override
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        if(state == GameState.Ready)
            updateReady(touchEvents);
        if(state == GameState.Running)
            updateRunning(touchEvents, deltaTime);
        if(state == GameState.Paused)
            updatePaused(touchEvents);
        if(state == GameState.GameOver)
            updateGameOver(touchEvents);
    }

    private void updateReady(List<TouchEvent> touchEvents) {
        if(touchEvents.size() > 0) state = GameState.Running;
    }

    private void getCi(TouchEvent event)
    {
        xi=event.x;
        yi=event.y;
    }
    private void getCf(TouchEvent event)
    {
        xf=event.x;
        yf=event.y;
        distancex=xf-xi;
        distancey=yf-yi;
    }
    private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                getCf(event);
                        if (distancey < -30) board.getKing().turnUp();
                        else if (distancey > 30) board.getKing().turnDown();
                        else if (distancex < -30) board.getKing().turnLeft();
                        else if (distancex > 30) board.getKing().turnRight();
                Assets.click.play(1);
                if (event.x<64 && event.y<64) {
                    state=GameState.Paused;
                }
                return;
            }
            if(event.type == TouchEvent.TOUCH_DOWN) {
                getCi(event);
            }
        }

       board.update(deltaTime);

        if(board.gameOver) {
            Assets.xoc.play(1);
            state = GameState.GameOver;
        }


    }

    private void updatePaused(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x > 80 && event.x <= 240) {
                    if(event.y > 100 && event.y <= 148) {
                        Assets.click.play(1);
                        state = GameState.Running;
                        return;
                    }
                    if(event.y > 148 && event.y < 196) {
                        Assets.click.play(1);
                        game.setScreen(new MainMenuScreen(game));
                        return;
                    }
                }
            }
        }
    }

    private void updateGameOver(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x >= 128 && event.x <= 192 &&
                        event.y >= 200 && event.y <= 264) {
                    Assets.click.play(1);
                    game.setScreen(new MainMenuScreen(game));
                    return;
                }
            }
        }
    }

    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.background, 0, 0);


        drawBoard(board);
        if(state == GameState.Ready)
            drawReadyUI();
        if(state == GameState.Running)
            drawRunningUI();
        if(state == GameState.Paused)
            drawPausedUI();
        if(state == GameState.GameOver)
            drawGameOverUI();

    }

    private void drawBoard(Board board) {
        Graphics g = game.getGraphics();
        King king = board.getKing();
        Row rows[] = board.rows;
        int x;
        int y;

        Pixmap rowPixmap = Assets.row;
        for(int i = 0; i < rows.length; i++) {
            x = rows[i].x;
            y = rows[i].y * 32;
            g.drawPixmap(rowPixmap, x, y);
        }
        g.drawPixmap(rowPixmap, 0, 0);
        g.drawPixmap(rowPixmap, 0, 14*32);

        Pixmap thronePixmap = Assets.throne;
        g.drawPixmap(thronePixmap, 1*32, 0);
        g.drawPixmap(thronePixmap, 4*32, 0);
        g.drawPixmap(thronePixmap, 5*32, 0);
        g.drawPixmap(thronePixmap, 8*32, 0);

        Pixmap chestPixmap = Assets.chest;
        g.drawPixmap(chestPixmap, 0, 0);
        g.drawPixmap(chestPixmap, 2*32, 0);
        g.drawPixmap(chestPixmap, 3*32, 0);
        g.drawPixmap(chestPixmap, 6*32, 0);
        g.drawPixmap(chestPixmap, 7*32, 0);
        g.drawPixmap(chestPixmap, 9*32, 0);

        Pixmap kingPixmap = null;
        switch (board.king_id) {
            case 0:
                kingPixmap = Assets.king1;
                break;
            case 1:
                kingPixmap = Assets.king2;
                break;
            case 2:
                kingPixmap = Assets.king3;
                break;
        }
        x = king.x * 32;
        y = king.y * 32;
        g.drawPixmap(kingPixmap, x, y);

        Pixmap noelPixmap = null;
        for(int i = 0; i < rows.length; i++) {
            if(rows[i].dir == false){
                noelPixmap = Assets.noel_left;
            }
            else{
                noelPixmap = Assets.noel_right;
            }
            for(int j = 0; j < rows[i].noels.length; j++) {
                x = rows[i].noels[j].x * 32;
                y = rows[i].noels[j].y * 32;
                g.drawPixmap(noelPixmap, x, y);
            }
        }


    }

    private void drawReadyUI() {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.ready, 47, 100);
    }

    private void drawRunningUI() {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.buttons, 0, 0, 64, 128, 64, 64);
    }

    private void drawPausedUI() {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.pause, 80, 100);
    }

    private void drawGameOverUI() {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.gameOver, 62, 100);
        g.drawPixmap(Assets.buttons, 128, 200, 0, 128, 64, 64);
    }

    @Override
    public void pause() {
        if(state == GameState.Running)
            state = GameState.Paused;
    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}

