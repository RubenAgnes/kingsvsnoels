package cat.escolapia.damviod.pmdm.KingsVsNoels;

import cat.escolapia.damviod.pmdm.framework.Pixmap;

/**
 * Created by ruben.agnes on 11/01/2017.
 */
public class Row {
    int x, y;
    boolean dir;
    Noel[] noels;

    public Row(int x, int y, boolean dir) {
        this.x = x;
        this.y = y;
        this.dir = dir;
        noels = new Noel[1];
    }

    public void createNoels(int _x1, int _x2){
        Noel noel1 = new Noel(_x1, this.y);
        //Noel noel2 = new Noel(_x2, this.y);
        noels[0] = noel1;
        //noels[1] = noel2;
    }

    void update(){
        noels[0].move(dir);
        //noels[1].move(dir);
    }
}
